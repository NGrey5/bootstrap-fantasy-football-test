import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAccountContentComponent } from './delete-account-content.component';

describe('DeleteAccountContentComponent', () => {
  let component: DeleteAccountContentComponent;
  let fixture: ComponentFixture<DeleteAccountContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAccountContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAccountContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
