import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProfileContentComponent } from './edit-profile-content.component';

describe('EditProfileContentComponent', () => {
  let component: EditProfileContentComponent;
  let fixture: ComponentFixture<EditProfileContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProfileContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProfileContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
