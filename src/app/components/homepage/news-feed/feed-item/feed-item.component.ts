import { Component, OnInit, Input } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';

@Component({
  selector: 'app-feed-item',
  templateUrl: './feed-item.component.html',
  styleUrls: ['./feed-item.component.css']
})
export class FeedItemComponent implements OnInit {

  @Input() url: string;

  data;
  apiKey: String = 'neqdr0vol4szfwin9sfikqjbuosud1ny2vfhssgt';

  orderBy = [
    {name: 'Publication Date', func: 'pubDate'},
    {name: 'Author', func: 'author'},
    {name: 'Title', func: 'title'}
  ];

  constructor(private http: Http) {
  }

  ngOnInit() {
    this.http.get(`https://api.rss2json.com/v1/api.json?rss_url=${this.url}&count=100`)
      .map(res => res.json())
      .subscribe(res => {
        this.data = res;
        console.log(res);
      });
  }

  reorderFeed(order: String) {
    this.http.get(`https://api.rss2json.com/v1/api.json?rss_url=${this.url}&order_by=${order}&count=100`)
    .map(res => res.json())
    .subscribe(res => {
      this.data = res;
      console.log(res);
    });
  }

}
