import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { AccountSettingsPageComponent } from './pages/account-settings-page/account-settings-page.component';
import { SettingsTabsComponent } from './components/account-settings/settings-tabs/settings-tabs.component';
import { ProfileCardComponent } from './components/account-settings/profile-card/profile-card.component';
import { UserPassContentComponent } from './components/account-settings/settings-tabs/user-pass-content/user-pass-content.component';
import { EditProfileContentComponent } from './components/account-settings/settings-tabs/edit-profile-content/edit-profile-content.component';
import { DeleteAccountContentComponent } from './components/account-settings/settings-tabs/delete-account-content/delete-account-content.component';
import { NewsFeedComponent } from './components/homepage/news-feed/news-feed.component';
import { FeedItemComponent } from './components/homepage/news-feed/feed-item/feed-item.component';


@NgModule({
  declarations: [
    AppComponent,
    AccountSettingsPageComponent,
    SettingsTabsComponent,
    ProfileCardComponent,
    UserPassContentComponent,
    EditProfileContentComponent,
    DeleteAccountContentComponent,
    NewsFeedComponent,
    FeedItemComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
