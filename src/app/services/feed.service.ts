import { Injectable } from '@angular/core';

@Injectable()
export class FeedService {

  private feeds = [
/*'https://angular-craft.com/feed/',
    'https://www.smashingmagazine.com/feed/',
    'https://feeds.feedburner.com/thoughtram',*/
    'http://www.espn.com/espn/rss/nfl/news'
  ];

  getUserFeeds() {
    return this.feeds;
  }

  constructor() { }

}
